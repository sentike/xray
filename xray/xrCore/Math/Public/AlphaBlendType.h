// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once


/** Various ways to interpolate TAlphaBlend. */
enum EAlphaBlendType
{
    ABT_Linear              =0,
    ABT_Cubic               =1,
    ABT_Sinusoidal          =2,
    ABT_EaseInOutExponent2  =3,
    ABT_EaseInOutExponent3  =4,
    ABT_EaseInOutExponent4  =5,
    ABT_EaseInOutExponent5  =6,
    ABT_MAX                 =7,
};


/** Turn a linear interpolated alpha into the corresponding AlphaBlendType */
FORCEINLINE float AlphaToBlendType(float InAlpha, uint8 BlendType)
{
	switch( BlendType )
	{
	case ABT_Sinusoidal         : return FMath::Clamp<float>((FMath::Sin(InAlpha * PI - HALF_PI) + 1.f) / 2.f, 0.f, 1.f);
	case ABT_Cubic              : return FMath::Clamp<float>(FMath::CubicInterp<float>(0.f, 0.f, 1.f, 0.f, InAlpha), 0.f, 1.f);
	case ABT_EaseInOutExponent2 : return FMath::Clamp<float>(FMath::InterpEaseInOut<float>(0.f, 1.f, InAlpha, 2), 0.f, 1.f);
	case ABT_EaseInOutExponent3 : return FMath::Clamp<float>(FMath::InterpEaseInOut<float>(0.f, 1.f, InAlpha, 3), 0.f, 1.f);
	case ABT_EaseInOutExponent4 : return FMath::Clamp<float>(FMath::InterpEaseInOut<float>(0.f, 1.f, InAlpha, 4), 0.f, 1.f);
	case ABT_EaseInOutExponent5 : return FMath::Clamp<float>(FMath::InterpEaseInOut<float>(0.f, 1.f, InAlpha, 5), 0.f, 1.f);
	}

	return InAlpha;
}