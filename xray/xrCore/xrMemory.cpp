#include "stdafx.h"
#pragma hdrstop

#include	"xrsharedmem.h"


xrMemory	Memory;
BOOL		mem_initialized	= FALSE;
bool		shared_str_initialized	= false;

// Processor specific implementations
extern		pso_MemCopy		xrMemCopy_x86;
extern		pso_MemFill		xrMemFill_x86;
extern		pso_MemFill32	xrMemFill32_x86;

xrMemory::xrMemory()
{
	mem_copy	= xrMemCopy_x86;
	mem_fill	= xrMemFill_x86;
	mem_fill32	= xrMemFill32_x86;
}

void	xrMemory::_initialize()
{
	stat_calls = 0;
	stat_counter = 0;

	g_pStringContainer = xr_new<str_container>();
	shared_str_initialized = true;
	g_pSharedMemoryContainer = xr_new<smem_container>();
}

void	xrMemory::_destroy()
{
	extern str_container				verbosity_filters;
	verbosity_filters.clean();

	xr_delete					(g_pSharedMemoryContainer);
	xr_delete					(g_pStringContainer);
	mem_initialized				= FALSE;

}

void	xrMemory::mem_compact	()
{
	RegFlushKey						( HKEY_CLASSES_ROOT );
	RegFlushKey						( HKEY_CURRENT_USER );
	_heapmin						( );
	HeapCompact						(GetProcessHeap(),0);
	if (g_pStringContainer)			g_pStringContainer->clean		();
	if (g_pSharedMemoryContainer)	g_pSharedMemoryContainer->clean	();
	if (strstr(Core.Params,"-swap_on_compact"))
		SetProcessWorkingSetSize	(GetCurrentProcess(),size_t(-1),size_t(-1));
}

// xr_strdup
char*			xr_strdup		(const char* string)
{	
	VERIFY	(string);
	u32		len			= u32(xr_strlen(string))+1	;
	char *	memory		= (char*)	Memory.mem_alloc( len
#ifdef DEBUG_MEMORY_NAME
		, "strdup"
#endif // DEBUG_MEMORY_NAME
	);
	CopyMemory		(memory,string,len);
	return	memory;
}

XRCORE_API		BOOL			is_stack_ptr		( void* _ptr)
{
	int			local_value		= 0;
	void*		ptr_refsound	= _ptr;
	void*		ptr_local		= &local_value;
	ptrdiff_t	difference		= (ptrdiff_t)_abs(s64(ptrdiff_t(ptr_local) - ptrdiff_t(ptr_refsound)));
	return		(difference < (512*1024));
}


#include <tbb/scalable_allocator.h>

//=====================================================================================================================

void* xrMemory::mem_alloc(size_t size)
{
	stat_calls++;
	_ASSERT(size > 0);
	R_ASSERT(size < 0x7fffFFFF);
	return scalable_malloc(size);
}

void xrMemory::mem_free(void* P)
{
	stat_calls++;
	scalable_free(P);
}

void* xrMemory::mem_realloc(void* P, size_t size)
{
	return scalable_realloc(P, size);
}

void	__stdcall	xrMemFill_x86(void* dest, int value, u32 count)
{

	memset(dest, int(value), count);
}

void	__stdcall	xrMemFill32_x86(LPVOID dest, u32 value, u32 count)
{
	u32*	ptr = (u32*)dest;
	u32*	end = ptr + count;
	for (; ptr != end; ) *ptr++ = value;
}

void	__stdcall xrMemCopy_x86(LPVOID dest, const void* src, u32 n)
{
	memcpy(dest, src, n);
}


//=====================================================================================================================

extern BOOL	g_bDbgFillMemory;
