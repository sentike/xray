// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
#ifndef stdafxH
#define stdafxH
#pragma once
#include "xrCore.h"
#include "xrCore_platform.h"

#include "Containers/Public/EnumAsByte.h"
#include "HAL/Public/Platform.h"
#include "HAL/Public/GenericPlatformMath.h"
#include "Misc/Public/CoreMiscDefines.h"

#include "Math/Public/UnrealMath.h"
#endif

#pragma hdrstop

