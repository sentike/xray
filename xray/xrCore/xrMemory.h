#ifndef xrMemoryH
#define xrMemoryH
#pragma once


typedef void	__stdcall	pso_MemFill(void* dest, int value, u32 count);
typedef void	__stdcall	pso_MemFill32(void* dest, u32 value, u32 count);
typedef void	__stdcall	pso_MemCopy(void* dest, const void* src, u32 count);

#	define DUMP_PHASE	do {} while (0)


class XRCORE_API		xrMemory
{
public:
	struct				mdbg {
		void*			_p;
		size_t 			_size;
		const char*		_name;
		u32				_dummy;
	};
public:
	xrMemory();
	void				_initialize();
	void				_destroy();

	u32					stat_calls;
	s32					stat_counter;
public:
	void				dbg_register(void* _p, size_t _size, const char* _name);
	void				dbg_unregister(void* _p);
	void				dbg_check();

	u32					mem_usage(u32* pBlocksUsed = NULL, u32* pBlocksFree = NULL);
	void				mem_compact();
	void				mem_counter_set(u32 _val) { stat_counter = _val; }
	u32					mem_counter_get() { return stat_counter; }


	void*				mem_alloc(size_t	size);
	void*				mem_realloc(void*	p, size_t size);
	void				mem_free(void*	p);

	pso_MemCopy*		mem_copy;
	pso_MemFill*		mem_fill;
	pso_MemFill32*		mem_fill32;
};

extern XRCORE_API	xrMemory	Memory;

#undef	ZeroMemory
#undef	CopyMemory
#undef	FillMemory
#define ZeroMemory(a,b)		Memory.mem_fill(a,0,b)
#define CopyMemory(a,b,c)	memcpy(a,b,c)			//. CopyMemory(a,b,c)
#define FillMemory(a,b,c)	Memory.mem_fill(a,c,b)


#include "xrMemory_subst_msvc.h"


template <class T>
IC T*		xr_alloc(u32 count) 
{
	return  (T*)Memory.mem_alloc(count * sizeof(T)); 
}

template <class T>
IC void		xr_free(T* &P) 
{
	if (P) 
	{
		Memory.mem_free((void*)P); P = NULL; 
	}; 
}

IC void*	xr_malloc(size_t size) 
{
	return	Memory.mem_alloc(size); 
}

IC void*	xr_realloc(void* P, size_t size) 
{
	return Memory.mem_realloc(P, size); 
}


// new(0)
template <class T>
IC	T*		xr_new()
{
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T();
}
// new(1)
template <class T, class P1>
IC	T*		xr_new(const P1& p1) {
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T(p1);
}
// new(2)
template <class T, class P1, class P2>
IC	T*		xr_new(const P1& p1, const P2& p2) {
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T(p1, p2);
}
// new(3)
template <class T, class P1, class P2, class P3>
IC	T*		xr_new(const P1& p1, const P2& p2, const P3& p3) {
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T(p1, p2, p3);
}
// new(4)
template <class T, class P1, class P2, class P3, class P4>
IC	T*		xr_new(const P1& p1, const P2& p2, const P3& p3, const P4& p4) {
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T(p1, p2, p3, p4);
}
// new(5)
template <class T, class P1, class P2, class P3, class P4, class P5>
IC	T*		xr_new(const P1& p1, const P2& p2, const P3& p3, const P4& p4, const P5& p5) {
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T(p1, p2, p3, p4, p5);
}
// new(6)
template <class T, class P1, class P2, class P3, class P4, class P5, class P6>
IC	T*		xr_new(const P1& p1, const P2& p2, const P3& p3, const P4& p4, const P5& p5, const P6& p6) {
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T(p1, p2, p3, p4, p5, p6);
}
// new(7)
template <class T, class P1, class P2, class P3, class P4, class P5, class P6, class P7>
IC	T*		xr_new(const P1& p1, const P2& p2, const P3& p3, const P4& p4, const P5& p5, const P6& p6, const P7& p7) {
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T(p1, p2, p3, p4, p5, p6, p7);
}
// new(8)
template <class T, class P1, class P2, class P3, class P4, class P5, class P6, class P7, class P8>
IC	T*		xr_new(const P1& p1, const P2& p2, const P3& p3, const P4& p4, const P5& p5, const P6& p6, const P7& p7, const P8& p8) {
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T(p1, p2, p3, p4, p5, p6, p7, p8);
}
// new(9)
template <class T, class P1, class P2, class P3, class P4, class P5, class P6, class P7, class P8, class P9>
IC	T*		xr_new(const P1& p1, const P2& p2, const P3& p3, const P4& p4, const P5& p5, const P6& p6, const P7& p7, const P8& p8, const P8& p9) {
	T* ptr = (T*)Memory.mem_alloc(sizeof(T));
	return new (ptr) T(p1, p2, p3, p4, p5, p6, p7, p8, p9);
}


template <bool _is_pm, typename T>
struct xr_special_free
{
	IC void operator()(T* &ptr)
	{
		void*	_real_ptr = dynamic_cast<void*>(ptr);
		ptr->~T();
		Memory.mem_free(_real_ptr);
	}
};

template <typename T>
struct xr_special_free<false, T>
{
	IC void operator()(T* &ptr)
	{
		ptr->~T();
		Memory.mem_free(ptr);
	}
};

template <class T>
IC	void	xr_delete(T* &ptr)
{
	if (ptr)
	{
		xr_special_free<xrcore::is_polymorphic<T>::result, T>()(ptr);
		ptr = NULL;
	}
}
template <class T>
IC	void	xr_delete(T* const &ptr)
{
	if (ptr)
	{
		xr_special_free<xrcore::is_polymorphic<T>::result, T>(ptr);
		const_cast<T*&>(ptr) = NULL;
	}
}


XRCORE_API	char* 	xr_strdup	(const char* string);


#	if !(defined(__BORLANDC__) || defined(NO_XRNEW))
	IC void*	operator new		(size_t size)		{	return Memory.mem_alloc(size?size:1);				}
	IC void		operator delete		(void *p)			{	xr_free(p);											}
	IC void*	operator new[]		(size_t size)		{	return Memory.mem_alloc(size?size:1);				}
	IC void		operator delete[]	(void* p)			{	xr_free(p);											}
#	endif



XRCORE_API void vminfo			(size_t *_free, size_t *reserved, size_t *committed);
XRCORE_API void log_vminfo		();
XRCORE_API u32	mem_usage_impl	(HANDLE heap_handle, u32* pBlocksUsed, u32* pBlocksFree);

#endif // xrMemoryH